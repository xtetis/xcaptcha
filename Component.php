<?php

namespace xtetis\xcaptcha;

/**
 * Компонент капчи
 */
class Component extends \xtetis\xengine\models\Component
{
    /**
     * Рендер блока с капчей и полем ввода, если это необходимо
     */
    public static function renderCaptchaBlock()
    {
        return self::renderBlock(
            'blocks/captcha_container',
            [],
        );
    }

    /**
     * Проверка капчи
     */
    public static function checkCaptcha($captcha = '')
    {
        $check_captcha = \xtetis\xcaptcha\models\MathCaptchaModel::checkCaptcha($captcha);
            
        // Сбрасываем капчу, чтобы повторно нельзя было вводить
        \xtetis\xcaptcha\models\MathCaptchaModel::flushSessionCaptchaValue();
        return $check_captcha;
    }

}
