<?php


/**
 * Генерация картинки капчи
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}



$model_math_captcha = new \xtetis\xcaptcha\models\MathCaptchaModel([]);
if (
    (!$model_math_captcha->setMathPhraseStr()) ||
    (!$model_math_captcha->renderCaptchaImage()) ||
    ($model_math_captcha->getErrors())
)
{
    die($model_math_captcha->getLastErrorMessage());
}