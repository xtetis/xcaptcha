class xcaptcha {
    /**
     * Перезагрузка картинки с капчей
     * 
     * @param {*} obj 
     */
    static reload(obj) {
        var original_src = $('.captcha_math_img').attr('original_src');

        var url = new URL(original_src);
        url.searchParams.set('rnd', genRandonString(10));
        $('.captcha_math_img').attr('src', url.toString());
        $('input[name=captcha]').val('');
    }
}


$(function() {




    // Клик по капче перезагружает её
    $('.captcha_math_img')
        .click(function() {
            xcaptcha.reload();
        });





});