<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }
    // Добавляем файл JS для работы с капчей
    \xtetis\xengine\helpers\StatResHelper::addTemplateJs(__DIR__ . '/../../web/js/xcaptcha.js');

    // Урл страницы капчи
    $url_captchaimg = \xtetis\xcaptcha\Component::makeUrl([
        'path'      => [
            'image',
            'math',
        ],
        'with_host' => true,
    ]);

?>
<div>
    <div class="input-group mb-4">
        <img src="<?=$url_captchaimg?>"
             original_src="<?=$url_captchaimg?>"
             class="captcha_math_img"
             srcset="">
    </div>
    <div class=" mb-4">
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i data-feather="shield"></i></span>
            </div>
            <input type="text"
                   name="captcha"
                   class="form-control"
                   placeholder="Решите пример на изображении">

        </div>
        <div class="error_form__captcha form_error_item"></div>
    </div>
</div>
