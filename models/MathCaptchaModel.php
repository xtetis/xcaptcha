<?php

namespace xtetis\xcaptcha\models;

class MathCaptchaModel extends \xtetis\xengine\models\Model
{
    /**
     * @var string
     */
    public $output_string = '';

    /**
     * @var mixed
     */

    public $math_count_elements = 3;

    /**
     * @var int
     */
    public $max_count_vertical_lines = 5;
    /**
     * @var int
     */
    public $max_thicknes_of_lines = 2;
    /**
     * @var int
     */
    public $max_count_horizontal_lines = 4;
    /**
     * @var int
     */
    public $max_random_pixels = 20;

    /**
     * @var string
     */
    public $captcha_string = '';

    /**
     * @var int
     */
    public $math_captcha_result = 0;

    /**
     * @var array
     */
    public $math_captcha_element_list = [];
    /**
     * @var array
     */
    public $math_captcha_action_list = [];

    /**
     * @param array $params
     */
    public function __construct($params = [])
    {

        if ($this->getErrors())
        {
            return false;
        }

        $allow_create_params = [
            'math_count_elements',
        ];

        foreach ($allow_create_params as $allow_create_params_item)
        {
            if (
                (isset($params[$allow_create_params_item])) &&
                (property_exists($this, $allow_create_params_item))
            )
            {
                $this->$allow_create_params_item = $params[$allow_create_params_item];
            }
        }

    }

    /**
     * @param $string
     */
    public function setCaptchaStr($string = '')
    {
        if ($this->getErrors())
        {
            return false;
        }
        $this->captcha_string = $string;
    }

    /**
     * @param $string
     */
    public function setSessionCaptchaValue()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->saveModel();
    }

    /**
     * @param $string
     */
    public static function flushSessionCaptchaValue()
    {
        self::flushModel();
    }

    /**
     * @param $string
     */
    public static function checkCaptcha($value)
    {
        $model = self::getModel(
            '',
            [],
            false
        );

        if (!$model)
        {
            return false;
        }

        if ($model->math_captcha_result != $value)
        {
            return false;
        }

        return true;
    }

    /**
     * @param array $params
     */
    public function setMathPhraseStr()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->math_count_elements = intval($this->math_count_elements);
        if ($this->math_count_elements < 2)
        {
            $this->math_count_elements = 2;
        }

        if ($this->math_count_elements > 4)
        {
            $this->math_count_elements = 4;
        }

        $elements = [];
        for ($i = 0; $i < $this->math_count_elements; $i++)
        {
            $this->math_captcha_element_list[$i] = rand(0, 20);
        }

        $actions = [];
        for ($i = 0; $i < ($this->math_count_elements - 1); $i++)
        {
            if (rand(0, 20))
            {
                $action = '+';
            }
            else
            {
                $action = '-';
            }
            $this->math_captcha_action_list[$i] = $action;
        }

        $captcha_str = '';
        $captcha_int = 0;
        for ($i = 0; $i < $this->math_count_elements; $i++)
        {
            if (0 == $i)
            {
                $captcha_str = strval($this->math_captcha_element_list[$i]);
                $captcha_int = $captcha_int + $this->math_captcha_element_list[$i];
            }
            else
            {
                $captcha_str = $captcha_str . strval($this->math_captcha_action_list[$i - 1]) . strval($this->math_captcha_element_list[$i]);
                if ('+' == $this->math_captcha_action_list[$i - 1])
                {
                    $captcha_int = $captcha_int + $this->math_captcha_element_list[$i];
                }
                else
                {
                    $captcha_int = $captcha_int - $this->math_captcha_element_list[$i];
                }
            }

        }

        $captcha_str               = $captcha_str . '=?';
        $this->math_captcha_result = $captcha_int;

        $this->setCaptchaStr($captcha_str);



        return $this->math_captcha_result;
    }

    public function renderCaptchaImage()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $filename = __DIR__ . '/../assets/img/captcha_bg/bg' . rand(0, 10) . '.png';

        if (!file_exists($filename))
        {
            $this->addError('filename', 'Файл подложки для капчи не найден');

            return false;
        }

        $dest = imagecreatefrompng($filename);
        imagealphablending($dest, false);
        imagesavealpha($dest, true);

        $text_colour = imagecolorallocate($dest, 0, 0, 255);

        $font_filename = __DIR__ . '/../assets/fonts/captcha.ttf';
        if (!file_exists($font_filename))
        {
            $this->addError('filename', 'Файл со шрифтом не найден не найден');

            return false;
        }

        $size                  = 17;
        $x_offset              = 18;
        $y_offset              = 30;
        $captcha_string_length = strlen($this->captcha_string);
        $symbol_width          = 13.5;

        $x_offset = ceil((150 - $captcha_string_length * $symbol_width) / 2);
        imagettftext($dest, $size, 0, $x_offset, $y_offset, $text_colour, $font_filename, $this->captcha_string);

        // Генерируем линии
        //=========================================================================
        if (($this->max_count_vertical_lines) && ($this->max_thicknes_of_lines))
        {
            $vertical_count_lines = rand(1, $this->max_count_vertical_lines);
            for ($i = 0; $i <= $vertical_count_lines; $i++)
            {
                $line_colour       = imagecolorallocate($dest, rand(0, 255), rand(0, 255), rand(0, 255));
                $thicknes_of_lines = rand(1, $this->max_thicknes_of_lines);
                imagesetthickness($dest, $thicknes_of_lines);
                $x1 = rand(0, 150);
                $y1 = 0;
                $x2 = rand(0, 150);
                $y2 = 50;
                imageline($dest, $x1, $y1, $x2, $y2, $line_colour);
            }
        }

        if (($this->max_count_horizontal_lines) && ($this->max_thicknes_of_lines))
        {
            $horizontal_count_lines = rand(1, $this->max_count_horizontal_lines);
            for ($i = 0; $i <= $horizontal_count_lines; $i++)
            {
                $line_colour       = imagecolorallocate($dest, rand(0, 255), rand(0, 255), rand(0, 255));
                $thicknes_of_lines = rand(1, $this->max_thicknes_of_lines);
                imagesetthickness($dest, $thicknes_of_lines);
                $x1 = 0;
                $y1 = rand(0, 50);
                $x2 = 150;
                $y2 = rand(0, 50);
                imageline($dest, $x1, $y1, $x2, $y2, $line_colour);
            }
        }
        //=========================================================================

        // Случайные пиксели на капче
        //=========================================================================
        for ($i = 0; $i < $this->max_random_pixels; $i++)
        {
            $pixel_color = imagecolorallocate($dest, rand(0, 255), rand(0, 255), rand(0, 255));
            imagesetpixel($dest, rand() % 150, rand() % 50, $pixel_color);
        }
        //=========================================================================

        header('Content-type: image/png');
        imagepng($dest);
        imagedestroy($dest);

        $this->setSessionCaptchaValue();

    }
}
