# xtetis\xcaptcha
## Модуль капчи

Powered by xTetis

Создает страницы:
 - Отрендеренная картинка капчи (/{CAPTCHA_COMPONENT_URL}/image/math)
---
## Установка модуля

 - Если Вы используете xEngine (https://bitbucket.org/xtetis/xengine/), то он содержит в ноде require файла composer.json, потому будет установлен автоматически
```sh
"xtetis/xcaptcha": "dev-master",
```
 - После установки модуля необходимо прописать следующие константы

| Константа | Тип     | Пример                | Описание                |
| :-------- | :------- | :------------------------- | :------------------------- |
| `COMPONENT_URL_LIST` | `строка, сериализованный массив` | define("COMPONENT_URL_LIST", serialize(['captcha' => 'xtetis\xcaptcha'])); | Список урлов модулей xEngine |

В шаблонах доступно использование капчи

```php
<?=\xtetis\xcaptcha\Component::renderCaptchaBlock();?>
```
При валидации формы
```php
// Проверка капчи
if (!\xtetis\xcaptcha\Component::checkCaptcha($this->captcha))
{
    $this->addError('captcha', 'Вы неверно прошли капчу');

    return false;
}
```
 -

## Обратная связь

Для связи с автором автором
skype: xtetis
telegram: @xtetis
