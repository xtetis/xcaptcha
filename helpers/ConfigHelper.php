<?php

namespace xtetis\xengine\helpers;

/**
 *
 */
class ConfigHelper
{


    /**
     * Возвращает массив из файла конфига
     *
     * @param  $name
     * @return mixed
     */
    public static function getConfigArray($config_name)
    {
        $ret      = [];
        $filename = ENGINE_DIRECTORY . '/config/' . $config_name . '.php';
        if (file_exists($filename))
        {
            $ret = require $filename;
        }

        return $ret;
    }

}
